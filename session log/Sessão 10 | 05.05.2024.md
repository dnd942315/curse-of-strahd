## Sessão 10 | 05.05.2024 

Encontramos o Strahd 

![strahd carriage](../assets/images/characters/WhatsApp%20Image%202024-05-05%20at%2016.40.42.jpeg)

Obrigou-nos a ajoelhar-nos 

Disse para “protegermos a Ireena” 

O Trukagul tentou agarrá-lo 

Abraça a Ireena, e ela abraça-o de volta 

Ele baza 

A Ireena achou o Strahd “a charming man” 

O Paraad sabe que ela está charmed e avisa-nos 

O Truk tentou dar uncharm, deu-lhe uma bofetada e não resultou 

O Frug começou a ladrar para as woods 

Gajo meio morto, a usar armadura com simbologia dracónica 

Undead Ranger, Level 5, HP 52 

![valdmir](../assets/images/players/WhatsApp%20Image%202024-05-14%20at%2016.51.04.jpeg)

Valdmir Gravetongue 

Parece estar a vigiar o Strahd 

Nunca viu dragonborns, odeia half-orcs 

Tem bronze draconic ancestry 

Vem de Orth, which is in another reality 

Seguimos caminho e vemos um moinho 

![Alt Text](../assets/images/locations/Old-Bonegrinder.gif)

![Alt Text](../assets/images/locations/Old-bonegrinder-megaliths.gif)

Vemos um corvo, que o Truk tenta chamar 

Corvo grita “Danger! Danger!” 

Nós aproximamo-nos do moinho 

Valthrun sobe com grappling hook 

Na primeira janela, vê uma velha a limpar o pó 

Na segunda janela, vê duas mulheres mais novas a dançar e a cantar em torno de um pequeno altar no centroda sala, como que a fazer encantamentos 

Valth desce e conta tudo à party 

Decidimos chamá-las 

Valth sobe de novo para a primera janela e esconde-se 

Paraad dá minor illusion de bater à porta 

Uma das mulheres mais novas desce e convida-nos a comer bolachas 

Entramos 

Truk manda poison à cara dela 

Roll for initiative 

Initiative: 
Valth 
Truk 
Paraad 
Bellin 
Ireena 
Valdmir 
Hags 

Íamos levar uma coça, mas o Valdmir deu deception para nos livrar da situação (nat 20), e elas falharam oinsight (nat 1) 

Bazámos 